import 'cypress-file-upload'

describe('Landing', () => {
  beforeEach(() => {
    cy.unregisterServiceWorkers()
    cy.visit('/')
  })
  it('have the correct titles and descriptions', () => {
    cy.title().should('include', 'MAT2')
    cy.get('h1').contains('Remove Metadata')
    cy.get('p').contains(
      'The file you see is just the tip of the iceberg. Remove the hidden metadata with MAT2'
    )
    cy.get('.uppy-Dashboard-AddFiles-title').contains(
      'Simply drag & drop, paste or use the file select button above.'
    )
    cy.get('[data-cy=footer-source-link]')
      .should('have.attr', 'href').and(
        'include',
        'https://0xacab.org/jfriedli/mat2-quasar-frontend'
      )
    cy.get('[data-cy=main-back-button]').should('not.be.visible')
  })
})

describe('Upload page tests', () => {
  beforeEach(() => {
    cy.unregisterServiceWorkers()
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/extension'
    }).as('api')
    cy.visit('/')
    cy.wait('@api').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'getting extensions successful')
    })
  })

  it('allows uploading a file', () => {
    cy.route({
      method: 'POST',
      url: '/api/upload'
    }).as('fileUpload')

    cy.get('.uppy-Dashboard-input').attachFile('test-image.png')
    cy.get('.uppy-Dashboard-Item').contains('test-image')
    cy.get('.uppy-StatusBar-actions > button').click()

    cy.wait('@fileUpload').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'file upload successful')
    })

    cy.get('[data-cy=removed-metadata-title]').contains('Metadata removed')
    cy.get('[data-cy=removed-metadata-paragraph]').contains('Successfully removed the metadata')
    cy.get('[data-cy=success-badge]').should('have.css', 'background-color')
      .and('eq', 'rgb(76, 175, 80)')
    cy.get('[data-cy=success-badge]').should('have.css', 'background-color')
      .and('eq', 'rgb(76, 175, 80)')
  })

  it('rejects not supported file extensions', () => {
    cy.get('.uppy-Dashboard-input').attachFile('not-supported.json')
    cy.wait(300)
    cy.get('.uppy-Informer').contains('You can only upload: .asc, .avi')
  })

  it('reject more than 10 files', () => {
    cy.get('.uppy-Dashboard-input')
      .attachFile('txt0.txt')
      .attachFile('txt1.txt')
      .attachFile('txt2.txt')
      .attachFile('txt3.txt')
      .attachFile('txt4.txt')
      .attachFile('txt5.txt')
      .attachFile('txt6.txt')
      .attachFile('txt7.txt')
      .attachFile('txt8.txt')
      .attachFile('txt9.txt')
      .attachFile('txt10.txt')
    cy.wait(300)
    cy.get('.uppy-Informer').contains('You can only upload 10 files ')
  })
})

describe('Getting Extensions', () => {
  beforeEach(() => {
    cy.unregisterServiceWorkers()
  })
  it('does fail and redirects to error page', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/extension',
      status: 400,
      response: ['']
    }).as('supportedExtensions')
    cy.visit('/')
    cy.wait('@supportedExtensions')
    cy.get('[data-cy=general-error-text]').contains('O Ooooh, Something went wrong')
  })
})
