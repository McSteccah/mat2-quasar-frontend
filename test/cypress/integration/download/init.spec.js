import 'cypress-file-upload'
describe('Download page tests', () => {
  beforeEach(() => {
    cy.unregisterServiceWorkers()
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/extension'
    }).as('api')
    cy.visit('/')
    cy.wait('@api').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'getting extensions successful')
    })
  })

  it('allows downloading multiple files', () => {
    cy.route({
      method: 'POST',
      url: '/api/upload'
    }).as('fileUpload')
    const fileNameOne = 'test-image.png'
    cy.get('.uppy-Dashboard-input').attachFile('test-image.png')
    cy.get('.uppy-Dashboard-files').contains(fileNameOne)

    const fileNameTwo = 'test-image-two.png'
    cy.get('.uppy-DashboardContent-addMore').click()
    cy.get('.uppy-Dashboard-input').attachFile(fileNameTwo)
    cy.get('.uppy-Dashboard-files').contains(fileNameTwo)
    cy.get('.uppy-StatusBar-actions > button').click({ force: true })

    cy.wait('@fileUpload').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'file 1 upload successful')
    })

    cy.get('[data-cy=removed-metadata-title]', { timeout: 7000 }).contains('Metadata removed')
    cy.get('[data-cy=removed-metadata-paragraph]').contains('Successfully removed the metadata')
    cy.get('[data-cy=main-back-button]').should('be.visible')
    cy.get('[data-cy=success-badge]').should('have.css', 'background-color')
      .and('eq', 'rgb(76, 175, 80)')

    cy.get('[data-cy=download-icon]').should('have.length', 2)
    cy.get('[data-cy=card-download-link]')
      .eq(0).should('have.attr', 'href').and(
        'include',
        'test-image.cleaned.png'
      )
    cy.get('[data-cy=card-download-link]')
      .eq(1).should('have.attr', 'href').and(
        'include',
        'test-image-two.cleaned.png'
      )
  })

  it('should display the removed/remaining metadata in a dialog', () => {
    cy.route({
      method: 'POST',
      url: '/api/*'
    }).as('fileUpload')

    const fileNameOne = 'eth_biwi_00546.pdf'
    cy.get('.uppy-Dashboard-input').attachFile(fileNameOne)
    cy.get('.uppy-Dashboard-files').contains(fileNameOne)
    cy.get('.uppy-StatusBar-actions > button').click()

    cy.wait('@fileUpload').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'file upload successful')
    })

    cy.get('[data-cy=metadata-menu-button]').eq(0)
      .click()
    cy.get('[data-cy=metadata-dialog-menu-entry-show]').eq(0)
      .click()

    cy.get('[data-cy=metadata-removed-table]')
      .contains('Acrobat Distiller 6.0 (Windows)')

    cy.get('[data-cy=metadata-remaining-table]')
      .contains('PDF-1.5')

    cy.get('[data-cy=metadata-dialog-close-button]').eq(0)
      .click()
    cy.get('[data-cy=metadata-removed-table]').should('not.be.visible')
  })

  it('should truncate the file name correctly', () => {
    cy.route({
      method: 'POST',
      url: '/api/*'
    }).as('fileUpload')

    const fileNameOne = 'a-very-long-name-for-a-very-small-text-file-to-truncate.txt'
    cy.get('.uppy-Dashboard-input').attachFile(fileNameOne)
    cy.get('.uppy-StatusBar-actions > button').click()
    cy.wait('@fileUpload').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'file upload successful')
    })

    cy.get('[data-cy=download-link]')
      .find('.q-btn__content')
      .contains('a-very-long-name...cate.cleaned.txt')
  })

  it('should display a download button for more than 4 files', () => {
    cy.get('.uppy-Dashboard-input')
      .attachFile('txt0.txt')
      .attachFile('txt1.txt')
      .attachFile('txt2.txt')
      .attachFile('txt3.txt')
      .attachFile('txt4.txt')
    cy.get('.uppy-StatusBar-actions > button').click()
    cy.get('[data-cy=failed-items-list]').should('not.be.visible')
    cy.get('[data-cy=zip-download-button] > .absolute-full > svg').should('be.visible')
    cy.get('[data-cy=zip-download-button]').contains('Download')
    cy.get('[data-cy=zip-download-button]').eq(0)
      .should('have.attr', 'href').and(
        'include',
        'cleaned.zip'
      )
  })
})

describe('Upload page negative tests', () => {
  beforeEach(() => {
    cy.unregisterServiceWorkers()
  })
  it('lists files that could no be cleaned', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/extension',
      response: ['.png', '.txt']
    }).as('supportedExtensions')
    cy.visit('/')
    cy.route({
      method: 'POST',
      url: '/api/upload',
      status: 400,
      response: {
        error: 'some random error'
      }
    }).as('postFile')
    cy.get('.uppy-Dashboard-input').attachFile('a-very-long-name-for-a-very-small-text-file-to-truncate.txt')
    cy.get('.uppy-StatusBar-actions > button').click()
    cy.wait('@postFile')
    cy.get('[data-cy=failed-file-name]').eq(0).contains('a-very-long...runcate.txt')
    cy.get('[data-cy=failed-items-list]').should('be.visible')
    cy.get('[data-removed-metadata-paragraph]').should('not.be.visible')
    cy.get('[data-cy=removed-metadata-title]').should('not.be.visible')
  })

  it('redirects to error page if zip creation fails', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/extension',
      response: ['.txt', '.jpg']
    }).as('supportedExtensions')
    cy.visit('/')
    cy.route({
      method: 'POST',
      url: '/api/download/bulk',
      status: 400,
      response: {
        error: 'some random error'
      }
    }).as('zipCreation')
    cy.get('.uppy-Dashboard-input')
      .attachFile('txt0.txt')
      .attachFile('txt1.txt')
      .attachFile('txt2.txt')
      .attachFile('txt3.txt')
      .attachFile('txt4.txt')
    cy.get('.uppy-StatusBar-actions > button').click()
    cy.wait('@zipCreation')
    cy.get('[data-cy=general-error-text]').contains('O Ooooh, Something went wrong')
  })
})
