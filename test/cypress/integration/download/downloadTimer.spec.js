import 'cypress-file-upload'

describe('Upload page download timer', () => {
  beforeEach(() => {
    cy.unregisterServiceWorkers()
  })
  it('should redirect to start page after expiring', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/extension',
      response: ['.png', '.txt']
    }).as('supportedExtensions')
    cy.visit('/')
    cy.route({
      method: 'POST',
      url: '/api/upload',
      status: 201,
      response: {
        inactive_after_sec: 2,
        output_filename: 'txt0.cleaned.txt',
        mime: 'text/plain',
        key: 'cb2d44b5f75b98cf3ff9b6bdc8ee673f244983387491db3b18cf20514217cfe9',
        secret: 'e4701206f4821993f0d54f9c6f2531aaa307e4f89c97a8679ba455e26aa3fc74',
        meta: {},
        meta_after: {},
        download_link: 'https://api.matweb.info/api/download/cb2d44b5f75b98cf3ff9b6bdc8ee673f244983387491db3b18cf20514217cfe9/e4701206f4821993f0d54f9c6f2531aaa307e4f89c97a8679ba455e26aa3fc74/txt0.cleaned.txt'
      }
    }).as('postFile')
    cy.get('.uppy-Dashboard-input').attachFile('a-very-long-name-for-a-very-small-text-file-to-truncate.txt')
    cy.get('.uppy-StatusBar-actions > button').click()
    cy.wait('@postFile')
    cy.wait(3000)
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq('/')
    })
  })

  it('should not redirect after leaving the page', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/extension',
      response: ['.png', '.txt']
    }).as('supportedExtensions')
    cy.visit('/')
    cy.route({
      method: 'POST',
      url: '/api/upload',
      status: 201,
      response: {
        inactive_after_sec: 2,
        output_filename: 'txt0.cleaned.txt',
        mime: 'text/plain',
        key: 'cb2d44b5f75b98cf3ff9b6bdc8ee673f244983387491db3b18cf20514217cfe9',
        secret: 'e4701206f4821993f0d54f9c6f2531aaa307e4f89c97a8679ba455e26aa3fc74',
        meta: {},
        meta_after: {},
        download_link: 'https://api.matweb.info/api/download/cb2d44b5f75b98cf3ff9b6bdc8ee673f244983387491db3b18cf20514217cfe9/e4701206f4821993f0d54f9c6f2531aaa307e4f89c97a8679ba455e26aa3fc74/txt0.cleaned.txt'
      }
    }).as('postFile')
    cy.get('.uppy-Dashboard-input').attachFile('a-very-long-name-for-a-very-small-text-file-to-truncate.txt')
    cy.get('.uppy-StatusBar-actions > button').click()
    cy.wait('@postFile')
    cy.visit('/info')
    cy.wait(3000)
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq('/info')
    })
  })
})
