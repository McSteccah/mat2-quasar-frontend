describe('404 page', () => {
  beforeEach(() => {
    cy.visit('/random-shizzle-1223')
  })
  it('should show the correct buttons and images', () => {
    cy.get('[data-cy=kitty-p]').find('img')
      .should('have.attr', 'src').and('include', 'img/kitty.')
    cy.get('[data-cy=four-o-four-text]').contains('There\'s no yarn here. (404)')

    cy.get('[data-cy=404-btn] .q-btn__content > span').contains('Back')
    cy.get('[data-cy=404-btn]')
      .should('have.attr', 'href').and(
        'include',
        '/'
      )
    cy.get('[data-cy=main-back-button]').should('be.visible')
  })
})
