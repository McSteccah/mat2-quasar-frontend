export function getBulkZipLink (state) {
  return state.bulkZipLink
}

export function getZipCreating (state) {
  return state.zipCreating
}
