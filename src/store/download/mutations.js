export function setZipCreating (state, isCreating) {
  state.zipCreating = isCreating
}
export function setBulkZipLink (state, link) {
  state.bulkZipLink = link
}
