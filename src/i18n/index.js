import en from './en.json'
import de from './de.json'
import fr from './fr.json'
import es from './es.json'
import it from './it.json'

export default {
  en: en,
  de: de,
  fr: fr,
  es: es,
  it: it
}
