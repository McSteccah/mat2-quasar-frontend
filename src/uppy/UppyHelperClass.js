import JSONUploader from './JSONUploader'
const Uppy = require('uppy')

/**
 * Handle uppy setup, config etc.
 */
export default class UppyHelperClass {
  /**
   * create a base instance
   * of an uppy uploader
   *
   * @param supportedExtensions
   * @param locale
   * @param target
   * @param uploadEndpoint
   * @returns {*}
   */
  static createUppyInstance (
    supportedExtensions = [],
    locale = {},
    target = '#drag-drop-area',
    uploadEndpoint = ''
  ) {
    const uppy = Uppy.Core({
      autoProceed: false,
      restrictions: {
        maxFileSize: process.env.MAX_UPLOAD_SIZE ? process.env.MAX_UPLOAD_SIZE : 16777216, // 16MB
        // the max standard for api/download/bulk endpoint
        maxNumberOfFiles: process.env.MAX_UPLOAD_FILES ? process.env.MAX_UPLOAD_FILES : 10,
        allowedFileTypes: supportedExtensions
      },
      locale: locale
    }).use(Uppy.Dashboard, {
      inline: true,
      target: target,
      height: '40vh',
      width: '100%',
      showProgressDetails: true,
      proudlyDisplayPoweredByUppy: false,
      showLinkToFileUploadResult: false
    }).use(Uppy.Webcam, {
      target: Uppy.Dashboard
    }).use(JSONUploader, {
      endpoint: uploadEndpoint
    })
    return uppy
  }
}
